﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionController : MonoBehaviour
{

    private ExplorerBehaviour explorerBehaviour;
    private ExplorerCompassController explorerCompass;

	// Use this for initialization
	void Start ()
    {
        explorerBehaviour = GetComponent<ExplorerBehaviour>();
        explorerCompass = GetComponent<ExplorerCompassController>();
    }
	
	void OnTriggerEnter(Collider other)
    {
        InteractionsInterface objectTriggered = other.GetComponent<InteractionsInterface>();

        objectTriggered.InteractExplorerCompass(explorerCompass);
        objectTriggered.InteractExplorer(explorerBehaviour);
    }

    void OnTriggerExit(Collider other)
    {
        InteractionsInterface objectTriggered = other.GetComponent<InteractionsInterface>();

        objectTriggered.EndOfInteractionExplorerCompass(explorerCompass);
        objectTriggered.EndOfInteractionExplorer(explorerBehaviour);
    }
}
