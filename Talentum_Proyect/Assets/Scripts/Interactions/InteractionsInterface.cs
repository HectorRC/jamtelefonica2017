﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface InteractionsInterface {

    /// <summary>
    /// This one is called 1st.
    /// </summary>
    void InteractExplorerCompass(ExplorerCompassController explorerCompass);

    /// <summary>
    /// This one is called 2nd.
    /// </summary>
    void InteractExplorer(ExplorerBehaviour explorer);

    /// <summary>
    /// Called 1st
    /// </summary>
    void EndOfInteractionExplorerCompass(ExplorerCompassController explorerCompass);

    /// <summary>
    /// Called 2nd
    /// </summary>
    void EndOfInteractionExplorer(ExplorerBehaviour explorer);
}
