﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle_Slower : MonoBehaviour, InteractionsInterface
{

    public float slowPercentage = 50.0f;

    public void InteractExplorerCompass(ExplorerCompassController explorerCompass)
    {
        return;
    }

    public void InteractExplorer(ExplorerBehaviour explorer)
    {
        explorer.SlowSpeed(slowPercentage);
    }

    public void EndOfInteractionExplorerCompass(ExplorerCompassController explorerCompass)
    {
        return;
    }

    public void EndOfInteractionExplorer(ExplorerBehaviour explorer)
    {
        explorer.ResetSpeed();
    }


    // Keep this in case we can use them latter

    // Use this for initialization
    void Start() { }

    // Update is called once per frame
    void Update() { }
}
