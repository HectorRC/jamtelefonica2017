﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle_Asessinate : MonoBehaviour, InteractionsInterface
{
    public void InteractExplorerCompass(ExplorerCompassController explorerCompass)
    {
        return;
    }

    public void InteractExplorer(ExplorerBehaviour explorer)
    {
        explorer.Die();
    }

    public void EndOfInteractionExplorerCompass(ExplorerCompassController explorerCompass)
    {
        return;
    }

    public void EndOfInteractionExplorer(ExplorerBehaviour explorer)
    {
        return;
    }


    // Keep this in case we can use them latter

    // Use this for initialization
    void Start() { }

    // Update is called once per frame
    void Update() { }
}
