﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entry_Interaction : MonoBehaviour, InteractionsInterface
{
    public void EndOfInteractionExplorer(ExplorerBehaviour explorer)
    {
        return;
    }

    public void EndOfInteractionExplorerCompass(ExplorerCompassController explorerCompass)
    {
        return;
    }

    public void InteractExplorer(ExplorerBehaviour explorer)
    {
        explorer.setInitialLuck();
    }

    public void InteractExplorerCompass(ExplorerCompassController explorerCompass)
    {
        return;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
