﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class City_Interaction : MonoBehaviour, InteractionsInterface
{

    public int lifes = 1;
    public delegate void FinishedGame();
    public event FinishedGame OnFinishedGame;

    public void InteractExplorerCompass(ExplorerCompassController explorerCompass)
    {
        lifes--;
        if(lifes <= 0)
        {
            OnFinishedGame();
            //TRIGGER WINING CONDITION
            Debug.LogError("YOU LOSE");
        }
    }

    public void InteractExplorer(ExplorerBehaviour explorer)
    {
        explorer.Die();
    }

    public void EndOfInteractionExplorerCompass(ExplorerCompassController explorerCompass)
    {
        return;
    }

    public void EndOfInteractionExplorer(ExplorerBehaviour explorer)
    {
        return;
    }


    // Keep this in case we can use them latter

    // Use this for initialization
    void Start() { }

    // Update is called once per frame
    void Update() { }
}
