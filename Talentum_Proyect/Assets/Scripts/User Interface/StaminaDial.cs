﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaDial : MonoBehaviour
{

    public Sprite active;
    public Sprite idle;

    private Image dial;

    void Awake()
    {
        dial = GetComponent<Image>();
    }

    void Start()
    {
        if (active == null)
        {
            Debug.LogError("No 'active' sprite for the stamina dial to display");
        }

        if (idle == null)
        {
            Debug.LogError("No 'idle' sprite for the stamina dial to display");
        }
    }

    public void Active()
    {
        dial.sprite = active;
    }

    public void Idle()
    {
        dial.sprite = idle;
    }

    public bool IsIdle()
    {
        return dial.sprite == idle;

    }


}
