﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{
    public float ScrollSpeed = 10f;
    public float ScrollEdge = 0.01f;

    // private int HorizontalScroll = 1;
    // private int VerticalScroll = 1;
    // private int DiagonalScroll = 1;

    public int PanSpeed = 100;

    private Vector2 ZoomRange = new Vector2(100, 1000);

    [SerializeField]
    private float CurrentZoom = 950.0f;
    public float ZoomZpeed = 10.0f;
    public float ZoomRotation = 0.0f;

    private Vector3 InitPos;
    private Vector3 InitRotation;



    void Start()
    {
        InitPos = transform.position;
        InitRotation = transform.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(2))
        {
            //(Input.mousePosition.x - Screen.width * 0.5)/(Screen.width * 0.5)

            transform.Translate(Time.deltaTime * PanSpeed * -(Input.mousePosition.x - Screen.width * 0.5f) / (Screen.width * 0.5f), 0, 0, Space.World);
            transform.Translate(0, 0, Time.deltaTime * PanSpeed * -(Input.mousePosition.y - Screen.height * 0.5f) / (Screen.height * 0.5f), Space.World);

        }
        else
        {
            if (Input.GetKey("a") || Input.mousePosition.x <= Screen.width * ScrollEdge)
            {
                transform.Translate(Vector3.right * Time.deltaTime * -ScrollSpeed, Space.World);
            }
            else if (Input.GetKey("d") || Input.mousePosition.x >= Screen.width * (1 - ScrollEdge))
            {
                transform.Translate(Vector3.right * Time.deltaTime * ScrollSpeed, Space.World);
            }

            if (Input.GetKey("s") || Input.mousePosition.y <= Screen.height * ScrollEdge)
            {
                transform.Translate(Vector3.forward * Time.deltaTime * -ScrollSpeed, Space.World);
            }
            else if (Input.GetKey("w") || Input.mousePosition.y >= Screen.height * (1 - ScrollEdge))
            {
                transform.Translate(Vector3.forward * Time.deltaTime * ScrollSpeed, Space.World);
            }
        }

        //ZOOM IN/OUT

        CurrentZoom -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * 1000 * ZoomZpeed;

        CurrentZoom = Mathf.Clamp(CurrentZoom, ZoomRange.x, ZoomRange.y);

        transform.position = transform.position - new Vector3(0, (transform.position.y - (InitPos.y + CurrentZoom) * 0.1f), 0);
        transform.eulerAngles = transform.eulerAngles - new Vector3(0, (transform.eulerAngles.x - (InitRotation.x + CurrentZoom * ZoomRotation)) * 0.1f, 0);

    }

}