﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplorerCompassUI : MonoBehaviour {

    Vector3 positionInitial;
    public Color blinkColor;
    public Transform needle;

    public float blinkingPeriod;
    private bool blinkCompass;
    private float timePassed;
    // Use this for initialization

    void Start () {
        positionInitial = transform.localPosition; 

	}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.localPosition = positionInitial;
        transform.rotation = Quaternion.Euler(0.0f, 270.0f, 0.0f);
        if (blinkCompass && timePassed >= blinkingPeriod)
        {
            if (gameObject.GetComponentInChildren<MeshRenderer>().material.color == blinkColor)
                gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.white;
            else
                gameObject.GetComponentInChildren<MeshRenderer>().material.color = blinkColor;
            timePassed = 0.0f;
        }
        else if (blinkCompass && timePassed < blinkingPeriod)
            timePassed += Time.fixedDeltaTime;
        else
        {
            timePassed = 0.0f;
            gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.white;
        }
    }

    public void setNeedleOrientation (float angle)
    {
        needle.eulerAngles = new Vector3(0.0f, angle, 0.0f);
    }

    public void toggleBlinking(bool value)
    {
        blinkCompass = value;
    }

    private IEnumerator blink()
    {
        while (true)
        {
            if (gameObject.GetComponentInChildren<MeshRenderer>().material.color == blinkColor)
                gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.white;
            else
                gameObject.GetComponentInChildren<MeshRenderer>().material.color = blinkColor;
            yield return new WaitForSeconds(0.8f);
        }
    }
}
