﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public static int explorerCounter;

    public StatsManager stats;
    public bool dynamicDifficulty;

    public GameManager manager;

    public GameObject explorer;
    public Collider spawnArea;
    public float baseSpawnRate;
    private float spawnRate;
    public MainCompasMovement mainCompass;
    public Transform objective;


    private List<ExplorerBehaviour> explorerList;
    //private IEnumerator spawnMethod;

    float xMax;
    float xMin;
    float zMax;
    float zMin;

    private float currentTimeCounter;

    // Defensive instruction
    void Awake()
    {
        explorerList = new List<ExplorerBehaviour>();
        explorerCounter = 0;
        if (objective == null)
        {
            Debug.LogError("Explorer objective not set in the 'Spawner' script.");
        }

        if (manager == null)
        {
            Debug.LogError("Manager not set in the 'Spawner' script.");
        }

        if (stats == null)
        {
            Debug.LogError("No stat manager on the 'Spawner' script.");
        }
    }

    // Use this for initialization
    void Start () {
        xMax = spawnArea.bounds.center.x + spawnArea.bounds.extents.x;
        xMin = spawnArea.bounds.center.x - spawnArea.bounds.extents.x;
        zMax = spawnArea.bounds.center.z + spawnArea.bounds.extents.z;
        zMin = spawnArea.bounds.center.z - spawnArea.bounds.extents.z;
        //spawnMethod = spawnExplorers();
        //StartCoroutine(spawnMethod);
    }

    private IEnumerator MakeThemDissapear()
    {
        foreach (ExplorerBehaviour explorer in explorerList)
        {
            if (explorer != null)
                explorer.Dissapear();
            yield return new WaitForSecondsRealtime(0.05f);
        }
    }
    /*public void initGame(
    {
        mainCompass.setInputEnabled(true);
    }

    public void finishGame() {
        StopCoroutine(spawnMethod);
    }*/

    // Update is called once per frame
    void Update()
    {
        if (manager.getState() == GameManager.GameState.started) { 
            if (explorerCounter < 1)
            {
                spawnOneExplorer();
                currentTimeCounter = 0.0f;
            }
            else
            {
                if (currentTimeCounter >= spawnRate)
                {
                    spawnOneExplorer();
                    currentTimeCounter = 0.0f;
                }
                else
                {
                    currentTimeCounter += Time.deltaTime;
                }
            }
        }
        if (manager.getState() == GameManager.GameState.finished) {
            StartCoroutine(MakeThemDissapear());
        }
	}


    void spawnOneExplorer()
    {
        Vector3 spawnPosition = Vector3.zero;
        float axis = Random.value;
        float minOrMax = Random.value;
        float positionAxis = Random.value;
        if (axis > 0.5) // set X
        {
            if (minOrMax > 0.5)
                spawnPosition.x = xMax;
            else
                spawnPosition.x = xMin;
            spawnPosition.z = zMin + positionAxis * (zMax - zMin);

        }
        else // set Z
        {
            if (minOrMax > 0.5)
                spawnPosition.z = zMax;
            else
                spawnPosition.z = zMin;
            spawnPosition.x = xMin + positionAxis * (xMax - xMin);
        }
        // Get difficulty from stats
        GameObject newExplorer = Instantiate(explorer, spawnPosition, Quaternion.identity);
        ExplorerCompassController explorerCompass = newExplorer.GetComponent<ExplorerCompassController>();
        explorerCompass.setMainCompass(mainCompass);
        ExplorerBehaviour explorerController = newExplorer.GetComponent<ExplorerBehaviour>();
        if (dynamicDifficulty)
        {
            DifficultyStats diffStats = stats.getDifficulty();
            spawnRate = baseSpawnRate * diffStats.spawnRate;
            explorerController.setSpeed(stats.getDifficulty().speedRate);
        }
        stats.RegisterExplorer(explorerController);
        manager.OnPauseState += explorerController.setIsPaused;
        //manager.OnGameFinished += explorerController.Dissapear;
        explorerController.setObjective(objective);
        explorerController.setMoving(true);
        explorerCounter++;
        explorerList.Add(explorerController);
        
    }

    /*private IEnumerator spawnExplorers()
    {
        while (true)
        {
            if (manager.getState() == GameManager.GameState.started)
            {
                spawnOneExplorer();
                yield return new WaitForSeconds(spawnRate);
            }
            else
            {
                yield return null;
            }
        }
    }*/
}
