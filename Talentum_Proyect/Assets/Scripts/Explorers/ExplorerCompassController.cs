﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplorerCompassController : MonoBehaviour {
    public float currentOrientation; //degrees: 0º is North
    public float currentDeviation;

    public MainCompasMovement mainCompass;
    public float updateRate = 1.0f; //Can act as difficulty setting

    private IEnumerator updateCompass;
    private ExplorerCompassUI _compassUI;

    public float getCurrentOrientation() { return (currentOrientation + currentDeviation) % 360; } //Used by Explorers

    public void setNewOrientation(float newOrientation) { currentOrientation = newOrientation; } //Used by explorers AI + internal call to deviation

    public void setOrientationDesviation(float desviation) { currentDeviation = desviation; } //Received from Main compass 
    
    public float getDeviation()
    {
        return currentDeviation;
    }
    
    void Awake ()
    {
        _compassUI = GetComponentInChildren<ExplorerCompassUI>();
        if (_compassUI == null)
        {
            Debug.LogError("ERROR: No compass UI added to explorer");
        }
    }
    // Use this for initialization
    void Start () {
        updateCompass = updateCompassOrientation();
        StartCoroutine(updateCompass);
    }
	
    //Use this to update needle?
	void FixedUpdate () {
        _compassUI.setNeedleOrientation(getCurrentOrientation());
	}

    public void setMainCompass(MainCompasMovement mainCompassScript)
    {
        mainCompass = mainCompassScript;
    }

    private IEnumerator updateCompassOrientation()
    {
        while (true)
        {
            float mainCompassDeviation = mainCompass.getOrientation();
            setOrientationDesviation(mainCompassDeviation);
            yield return new WaitForSeconds(updateRate);
        }
    }

    public void toggleBlinking(bool value)
    {
        _compassUI.toggleBlinking(value);
    }

}
