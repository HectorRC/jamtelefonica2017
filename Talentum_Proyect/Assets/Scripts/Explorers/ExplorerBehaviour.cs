﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ExplorerBehaviour : MonoBehaviour
{
    //DEBUG Text
    //private String[] names = new String[] { "S", "SW", "W", "NW", "N", "NE", "E", "SE" };

    public Transform objective;
    public float rotSpeed;
    public float initialMovementSpeed;
    public float distanceUntilCheck;
    public float closeToDecisionDistance;
    [Range (0.0f,1.0f)]
    public float smoothApproach;
    public float Luck = 1.0f;               //Deviation
    private float defaultLuck = 5000.0f;
    private float _movementSpeed;
    private float _rotSpeed;
    private Animator anim;
    private AudioController audio;

    [SerializeField]
    private float currentLuck;
    //Event used when an explorer dies
    public delegate void ExplorerDeath(float distance, float time);
    public event ExplorerDeath OnExplorerDeath;

    ExplorerCompassController _compassController;
    private float currentDistanceTraveled;
    private bool moving = false;
    private float[] possibleDirections;

    private bool isPaused = false;

    private Vector3 correctedSelfOrientation;

    void Awake()
    {
        ResetSpeed();

    }

    //Stats
    private float timeAlive = 0.0f;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        if(anim == null)
        {
            Debug.LogError("No animator in the explorer components");
        }
        currentLuck = defaultLuck;
        _rotSpeed = rotSpeed;
        correctedSelfOrientation = Vector3.forward;
        possibleDirections = new float[] { 180.0f,225.0f, 270.0f, 315.0f, 0.0f, 45.0f, 90.0f, 135.0f };
        currentDistanceTraveled = 0.0f;
        _compassController = GetComponent<ExplorerCompassController>();
        currentDistanceTraveled = distanceUntilCheck;
        audio = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPaused)
        {
            timeAlive += Time.deltaTime;
            if (moving)
            {
                selectOrientation();
            }
        }
    }

    void FixedUpdate()
    {
        if (!isPaused)
        {
            if (moving)
            {
                Move(_compassController.getCurrentOrientation());
            }
        }
    }

    public void setInitialLuck()
    {
        currentLuck = Luck;
    }

    public void setMoving(bool value)
    {
        moving = value;
    }

    public void setObjective(Transform newObjective)
    {
        objective = newObjective;
    }

    void Move(float targetOrientation)
    {
        //ROTATION (reference is targetOrientation)
        float orientation = targetOrientation % 360; //In case the Euler angle is > 360, unwind it
        float diff = orientation - transform.rotation.eulerAngles.y; //Get relative orientation from targetOrientation
        if (Mathf.Abs(diff) > ((_rotSpeed*0.1) * Time.fixedDeltaTime)) //Avoid weird twerking
        {
            if (diff < 0) //Avoid negative numbers
                diff += 360;

            if (diff - 180 >= 0) //Choose fastest rotation
            {
                transform.Rotate((-1) * Vector3.up, _rotSpeed * Time.fixedDeltaTime);
            }
            else
            {
                transform.Rotate(Vector3.up, _rotSpeed * Time.fixedDeltaTime);
            }
        }
        else
        {
            //Stop movement
            transform.rotation.eulerAngles.Set(transform.rotation.eulerAngles.x, orientation, transform.rotation.eulerAngles.z);
        }
        //Move forwards
        transform.Translate(Vector3.forward * Time.fixedDeltaTime * _movementSpeed);
        currentDistanceTraveled += Time.fixedDeltaTime * _movementSpeed;
    }


    void selectOrientation()
    {
        //Logic to determine orientation - based on dificulty (time played + eliminated explorers?)
        if (closeToDecisionDistance <= currentDistanceTraveled)
            _compassController.toggleBlinking(true);
        if (distanceUntilCheck <= currentDistanceTraveled)
        {
            float rng = UnityEngine.Random.value;
            //Debug.Log("RNG: " + rng);
            Vector3 realDirection = Vector3.Normalize(objective.transform.position - transform.position);
            //CORRECT
            correctedSelfOrientation = Quaternion.Euler(0,_compassController.getDeviation(),0) * Vector3.forward;
            
            //SMOOTH CHANGES
            //float absolutAngleExplorer = transform.rotation.eulerAngles.y;
            //float absolutAngleObjective = Vector3.Angle(transform.forward, realDirection);
            //realDirection = Quaternion.Euler(0.0f,absolutAngleObjective*smoothApproach,0.0f) * transform.forward;
            float[] alignment = new float[possibleDirections.Length];
            //Alignment
            for (int i=0; i<possibleDirections.Length; i++) {
                alignment[i] = (Vector3.Dot(Quaternion.Euler(0, possibleDirections[i], 0) * correctedSelfOrientation, realDirection) + 1.0f) / 2.0f;
                //Debug.Log("Alignment initial: " + alignment[i]);
            }
            //Adjust for luck
            for (int i = 0; i < alignment.Length; i++)
            {
                if (alignment[i] == Mathf.Max(alignment))
                    alignment[i] = alignment[i] * currentLuck;
                //Debug.Log("Alignment + Luck: "+alignment[i]);
            }
            //Get probabilities
            float[] probabilities = new float[possibleDirections.Length];
            for (int i = 0; i < alignment.Length; i++)
            {
                probabilities[i] = alignment.Where((value, index) => index <= i).ToArray().Sum() / alignment.Sum();
                if (probabilities[i] > rng) {
                    _compassController.setNewOrientation(possibleDirections[i]);
                    _compassController.toggleBlinking(false);
                    //Debug.Log("Explorer Decision: " + names[i] + "(" + possibleDirections[i] + ")");
                    currentDistanceTraveled = 0;
                    break;
                }
                //Debug.Log(probabilities[i]);
            }
            //Debug.Log(probabilities);
        }
    }


    // Terrain interaction.

    /// <summary>
    /// Destroy the explorer / triggers the death related routines
    /// </summary>
    public void Die()
    {
        Vector3 disVector = transform.position - objective.transform.position;
        float distance = Vector3.SqrMagnitude(disVector);
        OnExplorerDeath(distance, timeAlive);
        audio.DysplayDeath();
        // TODO animations & sounds of DEATH
        Destroy(this.gameObject);
    }

    public void Dissapear()
    {
        Destroy(this.gameObject);
    }

    /// <summary>
    /// Slows in a given % amount the speed of the explorer.
    /// </summary>
    public void SlowSpeed(float amount)
    {
        if (amount < 0.0f || amount > 100.0f)
        {
            Debug.LogError("The slow percentage of the 'Obstacle_Slower' component must be between 0 and 100");
        }
        anim.SetTrigger("Slowed");
        _movementSpeed = _movementSpeed * (100.0f - amount) / 100.0f;
    }

    /// <summary>
    /// Resets the speed of the explorer to his initial values.
    /// </summary>
    public void ResetSpeed()
    {
        _movementSpeed = initialMovementSpeed;
        if(anim != null)
        {
            anim.SetTrigger("UnSlowed");
        }
    }

    public void setIsPaused(bool value)
    {
        isPaused = value;
    }

    public void setSpeed(float speedRate)
    {
        _movementSpeed = initialMovementSpeed * speedRate;
        _rotSpeed = 10 * _movementSpeed;
    }

}
