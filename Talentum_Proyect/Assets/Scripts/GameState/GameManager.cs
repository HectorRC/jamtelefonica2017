﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    bool musicEnabled = true;
    bool soundFXEnabled = true;

    public AudioClip[] audioFiles;

    public MainCompasMovement compassController;
    public City_Interaction city;
    public StatsManager stats;

    public Canvas mainMenu;
    public Canvas pausedMenu;
    public Canvas credits;
    public Canvas finishedMenu;
    public Canvas layout;

    public Animator cameraAnimator;

    //Event used when an explorer dies
    public delegate void GamePaused(bool isPaused);
    public event GamePaused OnPauseState;



    public enum GameState {init, started, paused, finished};
    private GameState state;

    private AudioSource _source;

    public GameState getState()
    {
        return state;
    }

    public void StartGame(int i) {
        cameraAnimator.SetTrigger("InitGame");
        StartCoroutine(WaitForStartAnimation());
        StartCoroutine(playAfterMenu());
        mainMenu.enabled = false; // TO DO: Transition
    }

    private IEnumerator WaitForStartAnimation()
    {
        yield return new WaitForSeconds(2.0f);
        city.OnFinishedGame += FinishGame;
        if (!mainMenu.isActiveAndEnabled)
        {
            compassController.setInputEnabled(true);
            state = GameState.started;
            stats.enableStats(true);
            //OnPauseState(false);
        }
        layout.enabled = true;

    }

    IEnumerator playAfterMenu()
    {
        float length = _source.clip.length;
        float timeToPlay = _source.clip.length - _source.time;
        yield return new WaitForSeconds(timeToPlay);
        _source.clip = audioFiles[1];
        _source.Play();
    }

    private IEnumerator WaitForEndAnimation()
    {
        yield return new WaitForSeconds(4.0f);
        finishedMenu.enabled = true;
        stats.summarizeStats();
    }

    public void GotoMainMenu(int i)
    {
        //Transition!
        SceneManager.LoadScene(i);
    }

    void FinishGame()
    {
        cameraAnimator.SetTrigger("EndGame");
        StopCoroutine(playAfterMenu());
        StartCoroutine(WaitForEndAnimation());
        layout.enabled = false;
        _source.clip = audioFiles[2];
        //_source.loop = false;
        _source.Play();
        this.OnPauseState(true);
        state = GameState.finished; //Transition???
        compassController.setInputEnabled(false);
    }

    public void PauseGame(int i)
    {
        stats.enableStats(false);
        this.OnPauseState(true);
        pausedMenu.enabled = true;
        state = GameState.paused;
        compassController.setInputEnabled(false);
    }

    public void ResumeGame(int i)
    {
        stats.enableStats(true);
        OnPauseState(false);
        if (pausedMenu.isActiveAndEnabled)
        {
           pausedMenu.enabled = false;
           state = GameState.started;
        }
        compassController.setInputEnabled(true);
    }

    public void OpenCredits(int i)
    {
        if (state == GameState.init)
        {
            credits.enabled = true;
            mainMenu.GetComponent<GraphicRaycaster>().enabled = false;
        }
    }

    public void CloseCredits(int i)
    {
        credits.enabled = false;
        mainMenu.GetComponent<GraphicRaycaster>().enabled = true;
    }

    void ToggleMusic()
    {

    }

    void ToggleSoundEffects()
    {

    }

    void Awake ()
    {
        _source = GetComponent<AudioSource>();
    }
	// Use this for initialization
	void Start () {
        compassController.setInputEnabled(false);
        state = GameState.init;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (state == GameState.started)
            {
                PauseGame(-1);
            }
            else if (state == GameState.paused)
            {
                ResumeGame(-1);
            }
        }

		
	}
}
