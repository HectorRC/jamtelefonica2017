﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct GameStats
{
    public float timePlayed;
    public int killedExplorers;
    public float avgDistance;
    public float avgTime;
    public int score;
};

[System.Serializable]
public struct DifficultyStats
{
    public float spawnRate;
    public float luckRate;
    public float directionDistance;
    public float speedRate;
};

public class StatsManager : MonoBehaviour
{
    [Range(0.0f, 1.0f)]
    public float statsSpeed = 0.8f;

    //public float luckIncreasePercent = 0.05f;
    public float speedIncreasePercent = 0.05f;
    //public int explorersPerLuckIncrease = 10;
    public int timePerSpeedIncrease = 10;
    public float spawnRateDecreasePercent = 0.05f;
    public float decreaseSpawnRatePeriod = 10.0f;

    public GameStats stats;
    public DifficultyStats diffStats;

    public Text scoreText;
    public Text explorersKilledText;
    public Text timePlayedText;

    GameStats finishedStats;
    bool enabledStats = false;
    bool endingStats = false;

    int finishedTime = 0;
    int finishedKilled = 0;
    int finishedScore = 0;

    public void enableStats(bool value)
    {
        enabledStats = value;
    }

    public void summarizeStats()
    {
        enableStats(false);
        stats.score = (int)stats.timePlayed + 100 * stats.killedExplorers;
        finishedStats = stats;
        endingStats = true;
    }


    void showStats()
    {
        if ((int)finishedStats.timePlayed > 0)
        {
            float delta = 1.0f;
            if (finishedStats.timePlayed >= 10.0f)
                delta = Mathf.Pow(10, (Mathf.FloorToInt(Mathf.Log10(finishedStats.timePlayed)) - (1 - statsSpeed)));
            finishedStats.timePlayed -= (int)delta;
            finishedTime += (int)(delta);
            timePlayedText.text = finishedTime + " seconds";
        }
        else if (finishedStats.killedExplorers > 0)
        {
            float delta = 1.0f;
            if (finishedStats.killedExplorers >= 10.0f)
                delta = Mathf.Pow(10, (Mathf.FloorToInt(Mathf.Log10(finishedStats.killedExplorers)) - 1));
            finishedStats.killedExplorers -= (int)delta;
            finishedKilled += (int)(delta);
            explorersKilledText.text = ""+finishedKilled;
        }
        else if (finishedStats.score > 0.0f)
        {
            float delta = 1.0f;
            if (finishedStats.score >= 10.0f)
                delta = Mathf.Pow(10,(Mathf.FloorToInt(Mathf.Log10(finishedStats.score))-(1-statsSpeed)));
            finishedStats.score -= (int)delta;
            finishedScore += (int)(delta);
            scoreText.text = "" + finishedScore;
        }
    }

    public void RegisterExplorer(ExplorerBehaviour explorer)
    {
        explorer.OnExplorerDeath += AddExplorerDeathStats;
    }

    public void AddExplorerDeathStats(float distanceToObjective, float timeAlive) {
        Spawner.explorerCounter--;
        stats.killedExplorers++;
        stats.avgTime = (stats.avgTime * (stats.killedExplorers - 1) + timeAlive) / stats.killedExplorers;
        stats.avgDistance = (stats.avgDistance * (stats.killedExplorers - 1) + distanceToObjective) / stats.killedExplorers;
    }

    // TO DEPRECATE:
    public GameStats getGameStats()
    {
        return stats;
    }

    //TO DO:
    public DifficultyStats getDifficulty()
    {
        return diffStats;
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (enabledStats)
        {
            stats.timePlayed += Time.deltaTime;
            diffStats.spawnRate = 1.0f - spawnRateDecreasePercent * stats.timePlayed / decreaseSpawnRatePeriod;
            diffStats.speedRate = 1.0f + speedIncreasePercent * stats.timePlayed / decreaseSpawnRatePeriod; // 10 explorers killed equals a 5% increase on speed.
            diffStats.luckRate = 1.0f;
            //1.0f + luckIncreasePercent * stats.killedExplorers / (float)explorersPerLuckIncrease; // 10 explorers killed equals a 5% increase on luck.
            diffStats.directionDistance = 1;
        }
        if (endingStats)
        {
            showStats();
        }
            
    }
}
