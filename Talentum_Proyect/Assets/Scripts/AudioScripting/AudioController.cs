﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {

    public AudioClip death;
    public AudioClip dragStone;
    public AudioClip noStamina;
    public AudioClip clickMenu;
    public AudioClip score;

    [HideInInspector]
    public AudioSource player;

    // Use this for initialization
    void Start ()
    {
        player = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void DysplayDeath()
    {
        player.clip = death;
        player.Play();
    }

    public void DysplayDragStone()
    {
        //player.clip = dragStone;
        //player.Play();
    }

    public void DysplayNoStamina()
    {
        if(player.clip != noStamina)
        {
            player.clip = noStamina;
            player.Play();
        }
    }

    public void DysplayClickMenu()
    {
        player.clip = clickMenu;
        player.Play();
    }

    public void DysplayScore()
    {
        player.clip = score;
        player.Play();
    }

}
