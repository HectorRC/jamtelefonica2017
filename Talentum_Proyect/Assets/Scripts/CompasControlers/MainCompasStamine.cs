﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class wich manages the stamina pool, use and recovery of the compass dial movement.
/// </summary>
public class MainCompasStamine : MonoBehaviour
{
    public Image staminaBar; // UI image with visual stamina bar

    public float maxStamina = 100f;
    public float staminaDecreaseRatio = 1f;
    public float staminaMaintaingCapDecreaseRatio = 0.1f;
    public float staminaRecoverRatio = 1f;
    public float recoveryTime = 1.0f;

    private float _staminaPool;
    private float _internalRecoveryTimer;
    private StaminaDial dial;
    private AudioController audio;

    // Initialization before construction
    void Awake()
    {
        if(staminaBar == null) // Defensive error to remember manually link the UI with the script.
        {
            Debug.LogError("Set the reference of the UI stamina bar into the public variable 'staminaBar' of the MainCompasStamine script.");
        }

        _staminaPool = maxStamina;
        ResetRecovery();
    }

    void Start()
    {
        dial = GameObject.FindObjectOfType<StaminaDial>();
        audio = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioController>();
    }

    /// <summary>
    /// Returns false if theres not enought stamina left to use. It recieves the delta time of the frame.
    /// </summary>
    public bool HasStamina(float time)
    {
        if (_staminaPool <= 0.0f) // Returns false if stamina pool is empty
        {
            audio.DysplayNoStamina();
            Recover(time);
            return false;
        }
        else
        {
            return true;
        }
    }

    /// <summary>
    /// Uses stamina of the pool if able, used a a setter on stamina while spining the compass. Recieves delta time.
    /// </summary>
    public void UseStamina(float time)
    {
        dial.Active(); // Displays the stamina usage image
        AddStamina(-staminaDecreaseRatio * time);
        ResetRecovery();
    }

    /// <summary>
    /// Uses a fixed amount of stamina of the pool if able, used as a setter on stamina while maintaining the compass on his cap rotation. Recieves delta time.
    /// </summary>
    public void UseMaintainingCapStamina(float time)
    {
        dial.Active(); // Displays the stamina usage image
        AddStamina(-staminaMaintaingCapDecreaseRatio * time);
        ResetRecovery();
    }

    /// <summary>
    /// Recovers stamina after some time. It recieves the delta time of the frame.
    /// </summary>
    public void Recover(float time)
    {
        if (!dial.IsIdle())
            dial.Idle();

        if(_internalRecoveryTimer < recoveryTime)
            _internalRecoveryTimer += time;
        else if (_internalRecoveryTimer > recoveryTime)
        {
            AddStamina(staminaRecoverRatio * time);
        }
    }

    /// <summary>
    /// Resets the recovery time.
    /// </summary>
    public void ResetRecovery()
    {
        _internalRecoveryTimer = 0.0f;
    }

    public void AddStamina(float amount)
    {
        _staminaPool += amount;
        staminaBar.fillAmount = _staminaPool/100.0f;

        if (_staminaPool > maxStamina) _staminaPool = maxStamina;
        if (_staminaPool < 0.0f) _staminaPool = 0.0f;
    }
}
