﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class wich manages the main behaviour of the compass dial
/// </summary>
public class MainCompasMovement : MonoBehaviour
{
    public float rotSpeed = 10.0f; // Speed of rotation
    public float rotRecoverySpeed = 10.0f; // Speed of initial rotation recovery
    public float rotRecoveryTime = 1.0f;
    public float angleThreshold = 0.1f;

    public float maxAngle;

    private float lastAngle;
    private bool lastRotWasRight;
    private float _internalRecoveryTime;
    private MainCompasStamine _stamina;
    private bool inputEnabled = false;
    private AudioController audio;

    void Awake()
    {
        lastRotWasRight = true;
        lastAngle = 0.0f;
        ResetInternalTimer();
    }

	// Use this for initialization
	void Start ()
    {
        _stamina = GetComponent<MainCompasStamine>();
        rotRecoveryTime = _stamina.recoveryTime;
        audio = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioController>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        float time = Time.deltaTime;
        if (inputEnabled)
        {
            if (MovingRight(time) || MovingLeft(time)) // Checking input keys of the proyect. Uses time to avoid frame ratio changes.
            {

            }
            else
            {
                RecoverPosition(time);
                _stamina.Recover(time);
            }
        }
	}

    public void setInputEnabled(bool value)
    {
        inputEnabled = value;
    }

    private bool MovingRight(float time) // Checks if there is stamina avaible and uses it to spin clockwards
    {
       if (Input.GetButton("Right"))
            {
                if (_stamina.HasStamina(time))
                {
                    lastAngle = transform.rotation.eulerAngles.y;
                    transform.Rotate(Vector3.up, rotSpeed * time);
                    audio.DysplayDragStone();

                    if (transform.rotation.eulerAngles.y >= maxAngle && lastRotWasRight && lastAngle <= maxAngle)
                    {
                        //Debug.Log("Rotated MAX RIGHT");
                        CapFrameRotation(time,maxAngle);
                    }
                    else // Saves last rotation direction and uses stamina if isnt blocked by the rotation cap
                    {
                        lastRotWasRight = true;
                        RotatingFrame(time);
                        //Debug.Log("Rotating RIGHT");
                    }
                    return true;
                }
           }
        return false;
    }

    private bool MovingLeft(float time) // Checks if there is stamina avaible and uses it to spin counter-clockwards
    {
        if (inputEnabled)
        {
            if (Input.GetButton("Left"))
            {
                if (_stamina.HasStamina(time))
                {
                    lastAngle = transform.rotation.eulerAngles.y;
                    transform.Rotate(Vector3.up, -rotSpeed * time);
                    audio.DysplayDragStone();

                    if (transform.rotation.eulerAngles.y <= (360-maxAngle) && !lastRotWasRight && lastAngle >= (360-maxAngle))
                    {
                        //Debug.Log("Rotated MAX LEFT");
                        CapFrameRotation(time,(360-maxAngle));
                    }
                    else // Saves last rotation direction and uses stamina if isnt blocked by the rotation cap
                    {
                        lastRotWasRight = false;
                        RotatingFrame(time);
                        //Debug.Log("Rotating LEFT");
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private void RecoverPosition(float time)
    {
        if (_internalRecoveryTime < rotRecoveryTime)
            _internalRecoveryTime += time;
        else if (_internalRecoveryTime > rotRecoveryTime)
        {
            if (transform.rotation.eulerAngles.y > angleThreshold && transform.rotation.eulerAngles.y < (360.0f - angleThreshold)) // Detiene la rotación cuando apunta al norte con un threshold
            {
                if (transform.rotation.eulerAngles.y < maxAngle)
                {
                    transform.Rotate(Vector3.up, -rotRecoverySpeed * time);
                }
                else if (transform.rotation.eulerAngles.y > (360-maxAngle))
                {
                    transform.Rotate(Vector3.up, rotRecoverySpeed * time);
                }
                else
                {
                    if(lastRotWasRight)
                    {
                        transform.Rotate(Vector3.up, -rotRecoverySpeed * time);
                    }
                    else
                    {
                        transform.Rotate(Vector3.up, rotRecoverySpeed * time);
                    }
                }
            }
        }
    }

    private void CapFrameRotation(float time,float capAngle) // Maintains the compass frame on his cap rotation while pressing the move key. Requieres the time delta time as parameter.
    {
        Vector3 angle = transform.eulerAngles;
        angle.y = capAngle;
        transform.eulerAngles = angle;
        _stamina.UseMaintainingCapStamina(time);
        ResetInternalTimer();
    }

    private void RotatingFrame(float time) // Uses stamina when rotating the compass frame.
    {
        _stamina.UseStamina(time);
        ResetInternalTimer();
    }

    private void ResetInternalTimer()
    {
        _internalRecoveryTime = 0.0f;
    }

    public float getOrientation()
    {
        return transform.rotation.eulerAngles.y;
    }
}
